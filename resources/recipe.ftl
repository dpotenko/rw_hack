
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title>Albert Heijn Mail a friend</title>
</head>
<body>
<table cellspacing="0" cellpadding="0" border="0" width="320">
  <tr>
    <td height="70" valign="top">
      <a href="${recipeRequest.rootUrl}" target="_blank" style="display: block;">
        <img width="218" height="42" style="width: 218px; height: 42px; border: none;" src="${recipeRequest.imageUrlPrefix}/allerhande-print_657x128.png" alt="Allerhande"/>
      </a>
    </td>
  </tr>
  <tr>
    <td>
      <h2 style="font-size: 18px; font-weight: bold; font-family: Arial, Helvetica, sans; color: #000000;">Beste<#if recipeRequest.friendName??> ${recipeRequest.friendName}<#else>${recipeRequest.friendEmail}</#if>,</h2>
      <p style="font-size: 15px; line-height: 140%; font-family: Arial, Helvetica, sans; color: #000000; margin-bottom: 15px;">
        <#if recipeRequest.myName??>
          ${recipeRequest.myName} (${recipeRequest.myEmail})
        <#else >
          ${recipeRequest.myEmail}
        </#if>
        raadt je een recept aan:
      </p>
    </td>
  </tr>
  <tr>
    <td style="padding: 20px; background-color: #f0efea;">
      <#if (recipeRequest.recipe.recipeImage.imageRenditionUrls['445x297_JPG'])?has_content>
        <#assign recipeImage=recipeRequest.recipe.recipeImage.imageRenditionUrls['445x297_JPG'] />
      </#if>
      <h1 style="font-size: 18px; line-height: 140%; font-weight: bold; font-family: Arial, Helvetica, sans; color: #000000; margin-bottom: 15px;">${recipeRequest.recipe.title}</h1>
      <a href="${recipeRequest.recipeDetailPageUrl}?ns_campaign=allerhande_recept_delen&ns_mchannel=email&ns_source=allerhande&ns_linkname=ganaarditrecept" target="_blank" style="display:block;">
        <img width="280" height="187" style="width: 280px; height: 187px; border: none;" src="${recipeImage}" alt="${recipeRequest.recipe.title}"/>
      </a>
      <p style="font-size: 15px; font-family: Arial, Helvetica, sans; line-height: 30px; color: #000000;">
        <img width="16" height="16" src="${recipeRequest.imageUrlPrefix}/email/course.png"/>&nbsp;&nbsp;${recipeRequest.recipe.courses?join(", ")}<br/>
        <img width="16" height="16" src="${recipeRequest.imageUrlPrefix}/email/person.png"/>&nbsp;&nbsp;${recipeRequest.recipe.servingsNumber} ${recipeRequest.recipe.servingType}<br/>
        <#if recipeRequest.recipe.nutritions.energy??>
          <img width="16" height="16" src="${recipeRequest.imageUrlPrefix}/email/nutrition.png"/>&nbsp;&nbsp;${recipeRequest.recipe.nutritions.energy} kcal<br/>
        </#if>

        <br/>



        <#if recipeRequest.recipe.recipeTime.totalTime??>
          <img width="16" height="16" src="${recipeRequest.imageUrlPrefix}/email/time.png"/>&nbsp;&nbsp;${recipeRequest.recipe.recipeTime.totalTime} min. bereiden<br/>
          <br/>
        </#if>

        <#--<c:if test="${recipeRequest.not empty recipeCookTime}">-->
        <#--${recipeRequest.not isFirst ? timeIconPlaceholder : timeIconImage} ${recipeRequest.recipeCookTime} bereiden<br/>-->
        <#--<c:set var="isFirst" value="false"/>-->
        <#--</c:if>-->
        <#--<c:if test="${recipeRequest.not empty recipeWaitTime}">-->
        <#--${recipeRequest.not isFirst ? timeIconPlaceholder : timeIconImage} ${recipeRequest.recipeWaitTime} wachten<br/>-->
        <#--<c:set var="isFirst" value="false"/>-->
        <#--</c:if>-->
        <#--<c:if test="${recipeRequest.not empty recipeOvenTime}">-->
        <#--${recipeRequest.not isFirst ? timeIconPlaceholder : timeIconImage} ${recipeRequest.recipeOvenTime} oventijd<br/>-->
        <#--<c:set var="isFirst" value="false"/>-->
        <#--</c:if>-->
      </p>
    </td>
  </tr>
  <#if recipeRequest.message??>
    <tr>
      <td style="border-bottom: 1px dashed #B9B9B4;">
        <p style="font-size: 15px; font-family: Arial, Helvetica, sans; color: #000000; margin-bottom: 15px;"><br/>
          ${recipeRequest.message}
        </p>
      </td>
    </tr>
  </#if>
  <tr>
    <td>
      <p>
        <br/><a href="${recipeRequest.recipeDetailPageUrl}?ns_campaign=allerhande_recept_delen&ns_mchannel=email&ns_source=allerhande&ns_linkname=ganaarditrecept" target="_blank" style="font-family: Arial,sans; font-size: 18px; font-weight: bold; color: #6cbc88; text-decoration: underline;">ga naar dit Allerhande recept</a>
      </p>
    </td>
  </tr>
</table>
</body>
</html>
