function basicOp(operation, value1, value2) {
    switch (operation) {
        case '+':
            return value1 + value2;
        case '-':
            return value1 - value2;
        case '*':
            return value1 * value2;
        case '/':
            return value1 / value2;
    }
}

function printArray(array) {
    return array.join(',');
}

function getMiddle(s) {
    if (s.length % 2 === 0) {
        return s[s.length / 2 - 1] + s[s.length / 2]
    }
    return s[Math.floor(s.length / 2)];
}

function partitionOn(pred, items) {
    let index = 0;
    let i = 0;
    let fake = items;
    while (i < fake.length) {
        let bool = pred(items[i]);
        if (!bool) {
            let f = fake.slice(0, index);
            f.push(fake[i]);
            let rest = fake.slice(index);
            rest.splice(i - index, 1);
            f.push(...rest);
            fake = f;
            index++;
        }
        i++;
    }
    for (let j = 0; j < fake.length; j++) {
        items[j] = fake[j];
    }
    return index;
}

function array(arr) {
    let newArr = arr.split(',');
    newArr.shift();
    newArr.pop();
    if (newArr.length === 0) {
        return null;
    }
    return newArr.join(' ')
}

Array.prototype.filter = function (predicate) {
    let result = [];
    for (let el of this) {
        if (predicate(el)) {
            result.push(el);
        }

    }
    return result;
};

function prefill(n, v) {
    let number = Number(n);
    if (isNaN(Number.parseInt(n))) {
        throw TypeError(`${n} is invalid`);
    }
    if (!Number.isInteger(number) || number < 0) {
        throw TypeError(`${n} is invalid`)
    }
    if (number === 0) {
        return [];
    }

    let result = [];

    return Array(number).fill(v);
}


function crossProduct(vector1, vector2) {
    if (!(vector1 instanceof Array) || !(vector2 instanceof Array) || vector1.length !== 3 || vector2.length !== 3) {
        throw Error('Arguments are not 3D vectors!');
    }

    return [vector1[1] * vector2[2] - vector1[2] * vector2[1],
        vector1[2] * vector2[0] - vector1[0] * vector2[2],
        vector1[0] * vector2[1] - vector1[1] * vector2[0]]
}

function convert(input, source, target) {
    function toDecimal(input) {
        let result = 0;
        let col = [...input];
        for (let i = input.length - 1; i >= 0; i--) {
            result += source.indexOf(input[input.length - 1 - i]) * Math.pow(source.length, i);
        }
        return result;
    }

    let decimal = toDecimal(input);

    let result = [];
    let divisionResult = decimal;
    let remainder;
    while (true) {
        remainder = target[divisionResult % target.length];
        divisionResult = Math.trunc(divisionResult / target.length);
        result.push(remainder);
        if (divisionResult === 0) {
            break;
        }
    }

    return result.reverse().join('');
}

var Alphabet = {
    BINARY: '01',
    OCTAL: '01234567',
    DECIMAL: '0123456789',
    HEXA_DECIMAL: '0123456789abcdef',
    ALPHA_LOWER: 'abcdefghijklmnopqrstuvwxyz',
    ALPHA_UPPER: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    ALPHA: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
    ALPHA_NUMERIC: '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
};


var Cat = (function () {
    let totalWeight = 0;
    let numberOfCats = 0;
    let constructor = function (n, w) {
        if (n == null || w == null) {
            throw Error();
        }
        totalWeight += w;
        numberOfCats++;
        this.name = n;
        Object.defineProperty(this, 'weight', {
            set(newWeight) {
                totalWeight -= w;
                totalWeight += newWeight;
                w = newWeight;
            },

            get() {
                return w;
            }
        })
    };
    constructor.averageWeight = function () {
        return totalWeight / numberOfCats;
    };
    return constructor;
}());


function add(n) {
    function result(toAdd) {
        return add(n + toAdd);
    }

    result.valueOf = function () {
        return n;
    };
    result.toString = function () {
        return String(n);
    };
    return result;
}


function createMessage(str) {
    return function chain(newStr) {
        if (newStr == null || newStr.length === 0) {
            return str;
        }
        if (str.length !== 0) {
            str += ' ';
        }
        str += newStr;
        return chain;
    }
}

function cache(func) {
    let cachedArgs = [];
    return function () {
        for (let i = 0; i < cachedArgs.length; i++) {
            if (JSON.stringify(cachedArgs[i].args) === JSON.stringify(arguments)) {
                return cachedArgs[i].result;
            }
        }
        let result = func.apply(this, arguments);
        cachedArgs.push({args: arguments, result: result});
        return result;
    }
}


/*function compose(f, g) {
    return function () {
        return f.call(this, g.apply(this, arguments))
    }
}

add1 = function (a) {
    return a + 1
};
id = function (a) {
    return a
};*/

/*
console.log(compose(add1, id)(0));*/

function compose() {
    let funcs = arguments;
    return function (a) {
        let prevResult = a;
        for (let i = funcs.length - 1; i >= 0; i--) {
            prevResult = funcs[i].call(this, prevResult);
        }
        return prevResult;
    }
}

/*function spyOn(func) {
    let returnedValues = [];
    let passedParams = [];
    let callCount = 0;

    function wrapper() {
        callCount++;

        for (let i = 0; i < arguments.length; i++) {
            let items = JSON.stringify(arguments[i]);
            if (!passedParams.includes(items)) {
                passedParams.push(items);
            }
        }

        let result = func.apply(this, arguments);
        let resultJson = JSON.stringify(result);

        if (!returnedValues.includes(resultJson)) {
            returnedValues.push(resultJson);
        }

        return result;
    }

    wrapper.callCount = function () {
        return callCount;
    };
    wrapper.wasCalledWith = function (a) {
        return passedParams.includes(JSON.stringify(a));
    };
    wrapper.returned = function (a) {
        return returnedValues.includes(JSON.stringify(a));
    };

    return wrapper;
}*/


function spyOn(func) {
    let returnedValues = [];
    let passedParams = [];
    let callCount = 0;

    function wrapper() {
        callCount++;

        for (let i = 0; i < arguments.length; i++) {
            if (!passedParams.includes(arguments[i])) {
                passedParams.push(arguments[i]);
            }
        }

        let result = func.apply(this, arguments);

        if (!returnedValues.includes(result)) {
            returnedValues.push(result);
        }

        return result;
    }

    wrapper.callCount = function () {
        return callCount;
    };
    wrapper.wasCalledWith = function (a) {
        return passedParams.includes(a);
    };
    wrapper.returned = function (a) {
        return returnedValues.includes(a);
    };

    return wrapper;
}

function adder(n1, n2) {
    return n1 + n2;
}

var adderSpy = spyOn(adder);


function zero(f) {
    let value = 0;
    if (f == null) {
        return value;
    } else {
        return f(value);
    }
}

function one(f) {
    let value = 1;
    if (f == null) {
        return value;
    } else {
        return f(value);
    }
}

function two(f) {
    let value = 2;
    if (f == null) {
        return value;
    } else {
        return f(value);
    }
}

function three(f) {
    let value = 3;
    if (f == null) {
        return value;
    } else {
        return f(value);
    }
}

function four(f) {
    let value = 4;
    if (f == null) {
        return value;
    } else {
        return f(value);
    }
}

function five(f) {
    let value = 5;
    if (f == null) {
        return value;
    } else {
        return f(value);
    }
}

function six(f) {
    let value = 6;
    if (f == null) {
        return value;
    } else {
        return f(value);
    }
}

function seven(f) {
    let value = 7;
    if (f == null) {
        return value;
    } else {
        return f(value);
    }
}

function eight(f) {
    let value = 8;
    if (f == null) {
        return value;
    } else {
        return f(value);
    }
}

function nine(f) {
    let value = 9;
    if (f == null) {
        return value;
    } else {
        return f(value);
    }
}

function plus(number) {
    return function (another) {
        return another + number;
    }
}

function minus(number) {
    return function (another) {
        return another - number;
    }
}

function times(number) {
    return function (another) {
        return another * number;
    }
}

function dividedBy(number) {
    return function (another) {
        return Math.trunc(another / number);
    }
}

function nouveau(Constructor, ...args) {
    let instance = {};
    instance.__proto__ = Constructor.prototype;
    let constructed = Constructor.apply(instance, args);

    if (constructed == undefined || typeof constructed === 'string' || typeof constructed === 'boolean') {
        return instance;
    }
    return constructed;

}

function Person(name) {
    this.name = name;
}

Person.prototype.sayHi = function () {
    return 'Hi, I am ' + this.name;
};

var guy = nouveau(Person, 'Guy');

console.log(guy);

Object.prototype.hash = function (string) {
    let props = string.split('.');

    let currentVal = this;
    for (let i = 0; i < props.length; i++) {
        if (currentVal[props[i]]) {
            currentVal = currentVal[props[i]];
        } else {
            return;
        }
    }
    return currentVal;
};

var obj = {
    person: {
        name: 'joe',
        history: {
            hometown: 'bratislava',
            bio: {
                funFact: 'I like fishing.'
            }
        }
    }
};


/*numbers.square();  // must return [1, 4, 9, 16, 25]
numbers.cube();    // must return [1, 8, 27, 64, 125]
numbers.average(); // must return 3
numbers.sum();     // must return 15
numbers.even();    // must return [2, 4]
numbers.odd();*/
Array.prototype.square = function () {
    let copy = [];
    for (let i = 0; i < this.length; i++) {
        copy.push(this[i] * this[i]);
    }
    return copy;
};

Array.prototype.cube = function () {
    let copy = [];
    for (let i = 0; i < this.length; i++) {
        copy.push(this[i] * this[i] * this[i]);
    }
    return copy;
};

Array.prototype.average = function () {
    let average = (this.length || NaN) && 0;
    for (let i = 0; i < this.length; i++) {
        average += this[i];
    }
    return average / this.length;
};

Array.prototype.sum = function () {
    let sum = (this.length || NaN) && 0;
    for (let i = 0; i < this.length; i++) {
        sum += this[i];
    }
    return sum;
};

Array.prototype.even = function () {
    let copy = [];
    for (let i = 0; i < this.length; i++) {
        if (this[i] % 2 === 0) {
            copy.push(this[i]);
        }
    }
    return copy;
};

Array.prototype.odd = function () {
    let copy = [];
    for (let i = 0; i < this.length; i++) {
        if (this[i] % 2 === 1) {
            copy.push(this[i]);
        }
    }
    return copy;
};

class Animal {
    constructor(name, age, legs, species, status) {
        this.name = name;
        this.age = age;
        this.legs = legs;
        this.species = species;
        this.status = status;
    }

    introduce() {
        return `Hello, my name is ${this.name} and I am ${this.age} years old.`;
    }
}

class Shark extends Animal {
    constructor(name, age, status) {
        super(name, age, 0, 'shark', status);
    }
}

class Cat extends Animal {
    constructor(name, age, status) {
        super(name, age, 4, 'cat', status);
    }

    introduce() {
        return super.introduce() + '  Meow meow!';
    }
}

class Dog extends Animal {
    constructor(name, age, status, masterName) {
        super(name, age, 4, 'dog', status);
        this.masterName = masterName;
    }

    greetMaster() {
        return `Hello ${this.masterName}`
    }
}
