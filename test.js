function reversed(str) {
    var i = str.length - 1;
    var result = '';
    while (i >= 0) {
        result += str[i];
        i--;
    }
    return result;
}

let status = null;
if (status < 400 || status > 499) {
    console.log('Hello');
}

function removeChar(str) {
    let withoutFirst = str.substring(1, str.length);
    return withoutFirst.substring(0, str.length - 2)
};


function isPalindrome(line) {
    function reversed(str) {
        var i = str.length - 1;
        var result = '';
        while (i >= 0) {
            result += str[i];
            i--;
        }
        return result;
    }

    let str = '' + line;
    return str === reversed(str);
}

var isAnagram = function (test, original) {
    test = test.toLowerCase();
    original = original.toLowerCase();
    var i = 0;
    while (i < original.length) {
        if (test.indexOf(original[i]) >= 0) {
            test = test.replace(original[i], '')
        } else {
            return false;
        }
        i++;
    }
    if (test.length === 0) {
        return true;
    }
    return false;
};


function cake(x, y) {
    var alphabet = 'abcdefghijklmnopqrstuvwxyz';

    var result = 0;
    var i = 0;
    while (i < y.length) {
        if (i % 2 === 0) {
            result += y.charCodeAt(i);
        } else {
            result += alphabet.indexOf(y[i]) + 1;
        }
        i++;
    }
    if ((result / x) > 0.7) {
        return "Fire!"
    }
    return 'That was close!';
}


function century(year) {
    if (year % 100 > 0) {
        return Math.floor(year / 100) + 1
    } else {
        return Math.floor(year / 100)
    }
}


function mygcd(x, y) {
    while (y) {
        var prev = y;
        y = x % y;
        x = prev;
    }
    return x;
}

function factorial(n) {
    if (n < 0 || n > 12) {
        throw new RangeError();
    } else if (n === 0) {
        return 1;
    }
    return n * factorial(n - 1)
}

function findDup(arr) {
    var newArr = [];
    var i = 0;
    while (i < arr.length) {
        if (newArr.indexOf(arr[i]) >= 0) {
            return arr[i];
        } else {
            newArr.push(arr[i])
        }
        i++;
    }
}

function isPrime(num) {
    if (num < 2) {
        return false;
    }
    var i = 2;
    let end = Math.floor(Math.sqrt(num));
    while (i < end + 1) {
        if (num % i === 0) {
            return false;
        }
        i++;
    }
    return true;
}


function distinct(arr) {
    var newArr = [];
    var i = 0;
    while (i < arr.length) {
        if (newArr.indexOf(arr[i]) < 0) {
            newArr.push(arr[i])
        }
        i++;
    }
    return newArr;
}

function positiveSum(arr) {
    var result = 0;
    var i = 0;
    while (i < arr.length) {
        if (arr[i] > 0) {
            result += arr[i];
        }
        i++;
    }
    return result;
}


function fizzbuzz(n) {
    var result = [];
    var i = 1;
    while (i <= n.length) {
        var word = '';
        if (i % 3 === 0) {
            word += 'Fizz';
        }
        if (i % 5 === 0) {
            word += 'Buzz';
        }
        if (word.length === 0) {
            result.push(i);
        } else {
            result.push(word);
        }

        i++;

    }
    return result;
}

console.log(fizzbuzz([1, 2, 3, 4, 5, 6, 7]));

function multiplyAll(arr) {
    return function (coef) {
        var i = 0;
        var newArr = [];
        while (i < arr.length) {
            newArr.push(arr[i] * coef);
            i++;
        }
        return newArr;
    };
}


function findShort(s) {
    let split = s.split(' ');
    var shortest = Number.MAX_SAFE_INTEGER;
    for (var i = 0; i < split.length; i++) {
        if (split[i].length < shortest) {
            shortest = split[i].length;
        }
    }
    return shortest;
}

function squareDigits(num) {
    var str = '' + num;

    var result = '';
    for (var i = 0; i < str.length; i++) {
        let intVal = Number.parseInt(str[i]);
        result += intVal * intVal;
    }
    return Number.parseInt(result);
}

function processArray(arr, callback) {
    var i = 0;
    var newArr = [];
    while (i < arr.length) {
        newArr.push(callback(arr[i]));
        i++;
    }
    return newArr;
}


function count(string) {
    var result = {};
    for (var i = 0; i < string.length; i++) {
        if (result[string[i]] == null) {
            result[string[i]] = 1;
        } else {
            result[string[i]] = result[string[i]] + 1;
        }
    }

    return result;
}

function isLetter(str) {
    return str.match(/[a-zA-Z]/);
}
