package hacker

import hacker.Node
import java.util.*
import kotlin.math.abs

// Complete the maxMin function below.
class Node(val index: Int, val value: Int) : Comparable<Node> {
    override fun compareTo(other: Node): Int {
        if (this.value.compareTo(other.value) == 0) {
            return this.index.compareTo(other.index)
        }
        return this.value.compareTo(other.value)
    }

    override fun toString(): String {
        return this.value.toString()
    }


}

fun maxMin(k: Int, arr: Array<Int>): Int {
    val tree = TreeSet(arr.mapIndexed { index, value -> Node(index, value) })
    println(tree)
    val resultList = LinkedList<Node>()


    val first = tree.first()
    resultList.push(first)
    tree.remove(first)

    var best = Int.MAX_VALUE;
    while (tree.isNotEmpty()) {
        var current = resultList.peekLast()
        var minDif = Int.MAX_VALUE
        var nextIndex = 0
        var next = tree.ceiling(current)
        tree.remove(next)

        if (resultList.size < k) {
            best = abs(next.value - resultList.peekFirst().value)
        } else if (abs(next.value - resultList.get(1).value) < best) {
            best = abs(next.value - resultList.get(1).value)
        } else {
            println(resultList)
        }
        if (resultList.size == k) {
            resultList.poll()
        }
        resultList.add(next)
    }

    return best
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val n = scan.nextLine().trim().toInt()

    val k = scan.nextLine().trim().toInt()

    val arr = Array<Int>(n, { 0 })
    for (i in 0 until n) {
        val arrItem = scan.nextLine().trim().toInt()
        arr[i] = arrItem
    }

    val result = maxMin(k, arr)

    println(result)
}


class NewYearChaos {

}

/*

fun maxMin(k: Int, arr: Array<Int>): Int {
    val linkedList = LinkedList(arr.toList())
    linkedList.sort()
    println(linkedList)
    val resultList = LinkedList<Int>()


    resultList.push(linkedList.poll())

    var best = Int.MAX_VALUE;
    while (linkedList.isNotEmpty()) {
        var current = resultList.peekLast()
        var minDif = Int.MAX_VALUE
        var next = 0;
        var nextIndex = 0;
        for ((ind, el) in linkedList.withIndex()) {
            if (abs(current - el) < minDif) {
                next = el
                nextIndex = ind
                minDif = abs(current - el)
            }
        }
        linkedList.removeAt(nextIndex);
        if (resultList.size < k) {
            best = abs(next - resultList.peekFirst())
        } else if (abs(next - resultList.get(1)) < best) {
            best = abs(next - resultList.get(1))
        } else {
            println(resultList)
        }
        if (resultList.size == k) {
            resultList.poll()
        }
        resultList.add(next)
    }

    return best
}
*/
