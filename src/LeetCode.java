import java.awt.image.ImageProducer;
import java.util.HashMap;
import java.util.Map;

class Solution {

    public static void main(String[] args) {
        System.out.println(romanToInt("MCMXCIV"));
    }

    public  static int romanToInt(String s) {
        int sum = 0;
        HashMap<Character, SymbolProcessor> processorMap = new HashMap<>();

        processorMap.put('I', (prev) -> {
            if (prev == 'V') return -1;
            if (prev == 'X') return -1;
            return 1;
        });
        processorMap.put('X', (prev) -> {
            if (prev == 'L') return -10;
            if (prev == 'C') return -10;
            return 10;
        });
        processorMap.put('C', (prev) -> {
            if (prev == 'D') return -100;
            if (prev == 'M') return -100;
            return 100;
        });
        processorMap.put('V', (prev) -> 5);
        processorMap.put('L', (prev) -> 50);
        processorMap.put('D', (prev) -> 500);
        processorMap.put('M', (prev) -> 1000);

        char prev = 'I';
        for (int i = s.length()-1; i >=0; i--) {
            char currChar = s.charAt(i);
            sum += processorMap.get(currChar).process(prev);
            prev = currChar;
        }

        return sum;
    }

    interface SymbolProcessor {
        int process(char previous);
    }
}

//I, V, X, L, C, D and M
