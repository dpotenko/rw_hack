package package1;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


class Result {

    /*
     * Complete the 'optimalPoint' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY magic
     *  2. INTEGER_ARRAY dist
     */

    public static int optimalPoint(List<Integer> magic, List<Integer> dist) {
        int totalCurrentMagic = 0;
        int maxCapacity = 2 * magic.size() - 1;
        Integer[] simplifiedCircle = new Integer[maxCapacity];


        int startingPoint = -1;
        for (int i = 0; i < magic.size(); i++) {
            int routeCost = magic.get(i) - dist.get(i);
            simplifiedCircle[i] = routeCost;
            int reverseIndex = magic.size() + i;
            if (reverseIndex < simplifiedCircle.length) {

                simplifiedCircle[reverseIndex] = routeCost;
            }

            if (routeCost > 0 && startingPoint == -1) {
                startingPoint = i;
                totalCurrentMagic += routeCost;
            }
        }

        if (startingPoint == -1) {
            return -1;
        }

        int currentPoint = startingPoint;
        while (currentPoint < simplifiedCircle.length) {
            Integer currentMagic = simplifiedCircle[currentPoint];

            totalCurrentMagic += currentMagic;

            if (totalCurrentMagic < 0) {
                currentPoint++;
                startingPoint = currentPoint;
                totalCurrentMagic = 0;
                continue;
            } else if (currentPoint - startingPoint == magic.size() - 1) {
                return startingPoint;
            }

            currentPoint++;
        }
        return -1;
    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        final BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("input.txt")));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File("result.txt")));

        int magicCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> magic = new ArrayList<>();

        for (int i = 0; i < magicCount; i++) {
            int magicItem = Integer.parseInt(bufferedReader.readLine().trim());
            magic.add(magicItem);
        }

        int distCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> dist = new ArrayList<>();

        for (int i = 0; i < distCount; i++) {
            int distItem = Integer.parseInt(bufferedReader.readLine().trim());
            dist.add(distItem);
        }

        int result = Result.optimalPoint(magic, dist);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
