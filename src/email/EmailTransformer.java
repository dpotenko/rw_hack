package email;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class EmailTransformer {
    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory().enable(YAMLGenerator.Feature.MINIMIZE_QUOTES)
                .enable(YAMLGenerator.Feature.INDENT_ARRAYS)
                .disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER));
        HashMap<String, Object> yamlDef = mapper.readValue(new File("message-templates.yaml"), new TypeReference<HashMap<String, Object>>() {
        });


        File csvData = new File("pecfoff3-mail-message.csv");
        BufferedReader translation = new BufferedReader(new FileReader(new File("translation")));
        Map<String, String> translationMap = translation.lines()
                .map(line -> Pair.of(line.substring(0, line.indexOf("=")), line.substring(line.indexOf("=")).replace("=", "").trim())).collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
        CSVParser parser = CSVParser.parse(new FileReader(csvData), CSVFormat.RFC4180.withHeader("NO", "MMTY_CODE", "MAIL_SUBJECT", "START_DATE", "END_DATE", "MESSAGE_TEXT", "ATTACHMENT_FILENAME", "SENDER", "SHOC_NO"));
        Map<String, Map<String, Object>> messageTemplatesNode = (Map<String, Map<String, Object>>)
                yamlDef.get("/content/documents/administration/message-templates");
        Map<String, Object> ahMessages = messageTemplatesNode.get("/ah-messages");
        HashMap<String, Object> emailTemplate = (HashMap<String, Object>) ahMessages.get("/recipe");
        HashMap<String, Object> emailTemplateProperties = (HashMap<String, Object>) ahMessages.get("/recipe-properties");
        ahMessages.remove("/recipe");
        ahMessages.remove("/recipe-properties");

        for (CSVRecord csvRecord : parser) {
            String messageCode = csvRecord.get("MMTY_CODE");
            String shocNo = csvRecord.get("SHOC_NO");
            HashMap<String, Object> clonedEmailTemplate = SerializationUtils.clone(emailTemplate);
            HashMap<String, Object> clonedEmailTemplateProperties = SerializationUtils.clone(emailTemplateProperties);

            String messageText = csvRecord.get("MESSAGE_TEXT");
            String messageSubject = csvRecord.get("MAIL_SUBJECT");
            Pattern pattern = Pattern.compile("\\$\\w+\\$");
            Matcher matcherPlaceholdersBody = pattern.matcher(messageText);
            Matcher matcherPlaceholdersSubject = pattern.matcher(messageSubject);

            List<String> placeholders = matcherPlaceholdersBody.results().map(MatchResult::group).collect(Collectors.toList());
            placeholders.addAll(matcherPlaceholdersSubject.results().map(MatchResult::group).collect(Collectors.toList()));

            Set<String> camelCasePlaceHolders = placeholders.stream().map(placeholder -> {
                placeholder = placeholder.replace("$", "");
                if (StringUtils.isNoneBlank(translationMap.get(placeholder))) {
                    placeholder = translationMap.get(placeholder);
                }
                String camelCase = WordUtils.capitalizeFully(placeholder, new char[]{'_'}).replaceAll("_", "");
                camelCase = WordUtils.uncapitalize(camelCase);
                return camelCase;
            })
                    .collect(Collectors.toSet());

            for (String placeholder : placeholders) {
                String translatedPlaceholder = placeholder.replace("$", "");
                if (StringUtils.isNoneBlank(translationMap.get(translatedPlaceholder))) {
                    translatedPlaceholder = translationMap.get(translatedPlaceholder);
                }

                String camelCase = WordUtils.capitalizeFully(translatedPlaceholder, new char[]{'_'}).replaceAll("_", "");
                camelCase = WordUtils.uncapitalize(camelCase);
                String freemarkerCamelCase = "${" + camelCase.replace("$", "") + "}";
                messageText = messageText.replace(placeholder, freemarkerCamelCase);

                messageSubject = messageSubject.replace(placeholder, freemarkerCamelCase);
            }
            if (messageText.contains("<!DOCTYPE")) {
                messageText = messageText.substring(messageText.indexOf("<!DOCTYPE"));
            } else if (messageText.contains("<html")) {
                messageText = messageText.substring(messageText.indexOf("<html"));
            }

            String emailName = "/" + messageCode + "-" + shocNo;

            replaceVersion("/recipe-properties[1]", camelCasePlaceHolders, clonedEmailTemplateProperties, emailName);
            replaceVersion("/recipe-properties[2]", camelCasePlaceHolders, clonedEmailTemplateProperties, emailName);
            replaceVersion("/recipe-properties[3]", camelCasePlaceHolders, clonedEmailTemplateProperties, emailName);

            replaceEmail(clonedEmailTemplate, messageText, messageSubject, emailName, "/recipe[1]");
            replaceEmail(clonedEmailTemplate, messageText, messageSubject, emailName, "/recipe[2]");
            replaceEmail(clonedEmailTemplate, messageText, messageSubject, emailName, "/recipe[3]");

            ahMessages.put(emailName, clonedEmailTemplate);
            ahMessages.put(emailName + "-properties", clonedEmailTemplateProperties);
        }
        HashMap<Object, Object> result = new HashMap<>();
        result.put("/ah-messages", ahMessages);
        mapper.writeValue(new File("result.yaml"), result);
    }

    private static void replaceEmail(HashMap<String, Object> clonedEmailTemplate, String messageText, String messageSubject, String emailName, String oldEmailName) {
        HashMap<String, Object> clonedRecipeVersion = (HashMap<String, Object>) clonedEmailTemplate.get(oldEmailName);
        clonedRecipeVersion.put("ahonline:bodyTemplate", messageText);
        clonedRecipeVersion.put("ahonline:subjectTemplate", messageSubject);
        clonedRecipeVersion.put("ahonline:templateName", emailName.substring(1));
        clonedEmailTemplate.remove(oldEmailName);
        clonedEmailTemplate.put(emailName + oldEmailName.substring(oldEmailName.indexOf('[')), clonedRecipeVersion);
    }

    private static void replaceVersion(String propName, Set<String> camelCasePlaceHolders, HashMap<String, Object> clonedEmailTemplateProperties, String emailName) {
        HashMap<String, Object> clonedEmailTemplatePropertiesVersion = (HashMap<String, Object>) clonedEmailTemplateProperties.get(propName);
        clonedEmailTemplatePropertiesVersion.put("resourcebundle:keys", camelCasePlaceHolders);
        clonedEmailTemplatePropertiesVersion.put("resourcebundle:id", emailName.substring(1) + "-properties");
        clonedEmailTemplatePropertiesVersion.put("resourcebundle:messages", camelCasePlaceHolders.stream().map(val -> "'" + val + "'").collect(Collectors.toList()));
        clonedEmailTemplateProperties.remove(propName);
        clonedEmailTemplateProperties.put(emailName + propName.substring(propName.indexOf('-')), clonedEmailTemplatePropertiesVersion);
    }
}
