package ktl.stock

import com.google.gson.Gson
import java.io.File
import java.io.FileReader
import java.time.LocalDate
import java.time.chrono.IsoChronology
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoField

/*
9-June-2000
9-Jule-2014
Monday*/
fun main(args: Array<String>) {

    val startDate = readLine()
    val endDate = readLine()
    val weekDay = readLine()
    val gson = Gson()

    val fileReader = FileReader(File("data.json"))
    val stocks = gson.fromJson(fileReader, Array<Stock>::class.java)
    val stockPrice = StockPrice(stocks.toList())

    val foundStocks = try {
        stockPrice.openAndClosePrices(startDate, endDate, weekDay)
    } catch (e: InvalidParameterException) {
        println("Invalid parameter.")
        emptyArray<Stock>()
    } catch (e: NoStockException) {
        println("Stock not found.")
        emptyArray<Stock>()
    }

    foundStocks.forEach { stock ->
        println(stock.date + " " + stock.open + " " + stock.close)
    }

}

class InvalidParameterException : Exception()
class NoStockException : Exception()
class Stock {
    var date: String? = null
    var open: Double? = null
    var high: Double? = null
    var low: Double? = null
    var close: Double? = null
}

interface StockInterface {
    fun openAndClosePrices(firstDate: String?, lastDate: String?, weekDay: String?): Array<Stock>
}

class StockPrice : StockInterface {

    var stocks: MutableMap<String, MutableList<Stock>> = mutableMapOf()

    private val formatter = DateTimeFormatter.ofPattern("d-MMMM-yyyy")
        //  .withResolverStyle(ResolverStyle.STRICT)
        .withChronology(IsoChronology.INSTANCE)


    constructor(stocks: List<Stock>) {
        stocks.filter { stock -> stock.date != null }.forEach { stock ->
            if (this.stocks[stock.date] != null) {
                this.stocks[stock.date]?.add(stock)
            } else {
                this.stocks[stock.date!!] = mutableListOf(stock)
            }
        }

    }

    override fun openAndClosePrices(firstDate: String?, lastDate: String?, weekDay: String?): Array<Stock> {

        val min = LocalDate.of(2000, 1, 5)
        val max = LocalDate.of(2014, 1, 1)
        var firstParsedDate: LocalDate?
        var lastParsedDate: LocalDate?
        if (firstDate.isNullOrBlank() || firstDate == "null") {
            firstParsedDate = min
        } else {
            try {

                firstParsedDate = LocalDate.from(formatter.parse(firstDate))
            } catch (e: Exception) {
                throw InvalidParameterException()
            }
        }
        if (lastDate.isNullOrBlank() || lastDate == "null") {
            lastParsedDate = max
        } else {
            try {

                lastParsedDate = LocalDate.from(formatter.parse(lastDate))
            } catch (e: Exception) {
                throw InvalidParameterException()
            }
        }
        if (firstParsedDate < min) {
            firstParsedDate = min
        }

        if (lastParsedDate > max) {
            lastParsedDate = max
        }

        if (weekDay.isNullOrBlank()) {
            throw InvalidParameterException()
        }

        val daysOfWeek = listOf("MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY")
        val indexOfDay = daysOfWeek.indexOf(weekDay.trim().toUpperCase())
        if (indexOfDay == -1) {
            throw InvalidParameterException()
        }
        val weekDayInt = indexOfDay + 1


        val result = mutableListOf<Stock>()

        while (firstParsedDate!! <= lastParsedDate) {
            val currentDayOfWeek = firstParsedDate.get(ChronoField.DAY_OF_WEEK)
            var offset = 7
            val format = formatter.format(firstParsedDate)
            if (currentDayOfWeek != weekDayInt) {
                if (weekDayInt - currentDayOfWeek < 0) {
                    offset = 7 - currentDayOfWeek + weekDayInt
                } else {
                    offset = weekDayInt - currentDayOfWeek
                }
            } else {
                val stock = stocks[format]
                if (stock != null) {
                    result.addAll(stock)
                }
            }
            firstParsedDate = firstParsedDate.plusDays(offset.toLong())
        }

        if (result.isEmpty()) {
            throw NoStockException()
        }

        return result.toTypedArray()
    }
}