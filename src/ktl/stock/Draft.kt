/*
package ktl.stock

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import com.google.gson.*;
import java.time.format.DateTimeFormatter

fun main(args: Array<String>) {

    val startDate = readLine()
    val endDate = readLine()
    val weekDay = readLine()
    val gson = Gson()

    val stocks = gson.fromJson(FileReader(File("data.json")), Array<Stock>::class.java)

    val stockPrice = StockPrice(stocks.toList())

    val foundStocks = try {
        stockPrice.openAndClosePrices(startDate, endDate, weekDay)
    } catch (e: InvalidParameterException) {
        println("Invalid parameter.")
        emptyArray<Stock>()
    } catch (e: NoStockException) {
        println("Stock not found.")
        emptyArray<Stock>()
    }

    foundStocks.forEach { stock ->
        println(stock.date + " " + stock.open + " " + stock.close)
    }

}

class InvalidParameterException : Exception()
class NoStockException : Exception()
class Stock {
    var date: String? = null
    var open: Double? = null
    var high: Double? = null
    var low: Double? = null
    var close: Double? = null
}
interface StockInterface {
    fun openAndClosePrices(firstDate: String?, lastDate: String?, weekDay: String?): Array<Stock>
}
class StockPrice : StockInterface {

    var stocks: MutableMap<String, MutableList<Stock>> = mutableMapOf()

    private val simpleDateFormat = SimpleDateFormat("d-MMMMM-yyyy")

    init {
        simpleDateFormat.isLenient = false
    }

    constructor(stocks: List<Stock>) {
        stocks.filter { stock -> stock.date != null }.forEach { stock ->
            if (this.stocks[stock.date] != null) {
                this.stocks[stock.date]?.add(stock)
            } else {
                this.stocks[stock.date!!] = mutableListOf(stock)
            }
        }

    }

    override fun openAndClosePrices(firstDate: String?, lastDate: String?, weekDay: String?): Array<Stock> {
        if (firstDate == null || lastDate == null || weekDay == null) {
            throw InvalidParameterException()
        }


        val daysOfWeek = listOf("SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")
        val indexOfDay = daysOfWeek.indexOf(weekDay.toUpperCase())
        if (indexOfDay == -1) {
            throw InvalidParameterException()
        }
        val weekDayInt = indexOfDay + 1

        val firstParsedDate: Date?
        val lastParsedDate: Date?
        try {
            firstParsedDate = simpleDateFormat.parse(firstDate)
            lastParsedDate = simpleDateFormat.parse(lastDate)
        } catch (e: Exception) {
            throw InvalidParameterException()
        }

        val startCalendar = Calendar.getInstance()
        startCalendar.time = firstParsedDate

        val endCalendar = Calendar.getInstance()
        endCalendar.time = lastParsedDate

        val result = mutableListOf<Stock>()

        while (startCalendar.time <= endCalendar.time) {
            val currentDayOfWeek = startCalendar.get(Calendar.DAY_OF_WEEK)
            var offset = 7
            val format = simpleDateFormat.format(startCalendar.time)
            if (currentDayOfWeek != weekDayInt) {
                if (weekDayInt - currentDayOfWeek < 0) {
                    offset = 7 - currentDayOfWeek + weekDayInt
                } else {
                    offset = weekDayInt - currentDayOfWeek
                }
            } else if (currentDayOfWeek == weekDayInt) {
                val stock = stocks[format]
                if (stock != null) {
                    result.addAll(stock)
                }
            }
            startCalendar.add(Calendar.DAY_OF_WEEK, offset)
        }

        if (result.isEmpty()) {
            throw NoStockException()
        }

        return result.toTypedArray()
    }
}

*/
