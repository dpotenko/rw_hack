package ktl.price

import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.util.*


// Complete the maxInversions function below.
fun maxInversionsSuperFast(prices: Array<Int>): Long {

    var totalResult = 0L

    val consideredPrices = mutableListOf<Node>()

    for ((index, price) in prices.withIndex()) {
        for (consideredPrice in consideredPrices) {
            if (price < consideredPrice.value) {
                consideredPrice.lowerElementsAfter++
            }
        }
        consideredPrices.add(Node(price, index))
    }

    for ((index, price) in prices.withIndex()) {
        for (node in consideredPrices) {
            if (index < node.index && price > node.value) {
                totalResult += node.lowerElementsAfter
            }
        }
    }

    return totalResult
}

class Node(val value: Int, val index: Int) {
    var lowerElementsAfter: Int = 0
}


fun main(args: Array<String>) {
    val br = BufferedReader(FileReader(File("input.txt")))
    val pricesCount = br.readLine()!!.trim().toInt()

    val prices = Array<Int>(pricesCount, { 0 })
    for (i in 0 until pricesCount) {
        val pricesItem = br.readLine()!!.trim().toInt()
        prices[i] = pricesItem
    }

    val res = maxInversionsSuperFast(prices)

    println(res)
    br.close()
}


// Complete the maxInversions function below.
fun maxInversions(prices: Array<Int>): Long {

    var totalResult = 0L

    for ((index, price) in prices.withIndex()) {
        val lowerElements = mutableListOf<Int>()
        val nextElementIndex = index + 1
        var result = 0L

        if (nextElementIndex == prices.size) {
            break
        }
        for (i in nextElementIndex until prices.size) {
            if (prices[i] < price) {
                for (lower in lowerElements) {
                    if (prices[i] < lower) {
                        result++
                    }
                }
                lowerElements.add(prices[i])
            }
        }
        totalResult += result
    }

    return totalResult
}

fun maxInversionsFast(prices: Array<Int>): Long {

    var totalResult = 0L

    var pairs = mutableListOf<Pair<Node, Int>>()
    val consideredPrices = mutableListOf<Node>()

    for ((index, price) in prices.withIndex()) {
        for (consideredPrice in consideredPrices) {
            if (price < consideredPrice.value) {
                pairs.add(consideredPrice to price)
            }
        }
        consideredPrices.add(Node(price, index))
    }

    val sortedPairs = TreeMap<Int, MutableList<Pair<Node, Int>>>()

    for (pair in pairs) {
        var list = sortedPairs.get(pair.first.index)
        if (list == null) {
            sortedPairs.put(pair.first.index, mutableListOf(pair))
        } else {
            list.add(pair)
        }
    }

    for ((index, price) in prices.withIndex()) {
        if (index + 1 >= prices.size - 1) {
            continue
        }
        val subMap = sortedPairs.subMap(index + 1, prices.size - 1)
        for (listOfPairs in subMap.values) {
            if (price > listOfPairs[0].first.value) {
                totalResult += listOfPairs.size
            }
        }
    }

    return totalResult
}
