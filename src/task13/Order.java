package task13;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Order {
    public Long id;

    public List<Item> items = new ArrayList<>();

    public LocalDateTime creationDate;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", items=" + items +
                ", creationDate=" + creationDate +
                '}';
    }
}
