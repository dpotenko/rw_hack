package task13;

import java.time.LocalDate;

public class Item {

    public Long id;
    public String name;
    public LocalDate manufactureDate;

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", manufactureDate=" + manufactureDate +
                '}';
    }
}
