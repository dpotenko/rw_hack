package task13;

public class Solution {

    private static int[][] input;
    private static int[][] output;

    private static int maxj;
    private static int maxi;

    public static void main(String[] args) {


        int i = 0;
        for (int[] row : input) {
            int j = 0;
            for (int element : row) {
                input[i][j] = element;
                j++;
            }
            i++;
        }

        i = 0;
        for (int[] row : input) {
            int j = 0;
            for (int element : row) {
                if (element == 0) {
                    makeZero(i, j, Direction.IMINUS);
                    makeZero(i, j, Direction.IPLUS);
                    makeZero(i, j, Direction.JMINUS);
                    makeZero(i, j, Direction.JPLUS);
                }
                j++;
            }
            i++;
        }
    }

    private static void makeZero(int i, int j, Direction direction) {
        while (i < maxi && i >= 0 && j < maxj && j >= 0) {
            output[i][j] = 0;
            switch (direction) {
                case IPLUS: i++;
                case JMINUS: j--;
            }
        }
    }
}

enum Direction {
    IPLUS,
    JPLUS,
    JMINUS,
    IMINUS
}

// 1  = (n + m) x n x m = mn^2 + nm^2
