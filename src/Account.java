import java.math.BigDecimal;

public class Account {
    private BigDecimal money;


    public  void receive(BigDecimal amount) {
        money = money.add(amount);
    }

    public  void subtract(BigDecimal amount) {
        if (money.compareTo(amount) < 0) {
            throw new IllegalStateException("Not enough money!");
        }
        money = money.subtract(amount);
    }
}
