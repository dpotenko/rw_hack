package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Recipe {
    private RecipeImage recipeImage;
    private List<String> courses;
    private Integer servingsNumber;
    private String servingType;
    private Nutritions nutritions;
    private RecipeTime recipeTime;
    private String title;
}
