package model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecipeRequest {
    private String rootUrl;
    private String myName;
    private String myEmail;
    private String friendName;
    private String friendEmail;
    private String recipeDetailPageUrl;
    private String message;
    private String imageUrlPrefix;
    private Recipe recipe;
}
