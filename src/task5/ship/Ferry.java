package task5.ship;

import task5.container.Container;

public abstract class Ferry {
    protected Container[] containers;

    protected int smallContainersCount = 0;

    public Ferry() {
        containers = new Container[getMaxContainersCount()];
    }

    public void addContainer(Container container) {
        if (isEnoughCapacity(container)) {
            if (container.isBig()) {
                smallContainersCount += 2;
            } else {
                smallContainersCount++;
            }
            addContainerToArray(container);
        } else {
            System.out.println("Not enough space for a container " + container);
        }
    }

    protected abstract boolean isEnoughCapacity(Container container);

    protected abstract int getMaxContainersCount();

    private void addContainerToArray(Container container) {
        for (int i = 0; i < containers.length; i++) {
            if (containers[i] == null) {
                containers[i] = container;
                break;
            }
        }
    }

    public double getTotalWeight() {
        double totalWeight = 0;
        for (Container container : containers) {
            if (container != null) {
                totalWeight += container.calculateWeight();
            }
        }
        return totalWeight;
    }

}
