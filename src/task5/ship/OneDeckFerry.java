package task5.ship;

import task5.container.Container;

public class OneDeckFerry extends Ferry {
    @Override
    protected boolean isEnoughCapacity(Container container) {
        if (container.isBig() && smallContainersCount > 2) {
            return false;
        } else if (!container.isBig() && smallContainersCount == 4) {
            return false;
        }
        return true;
    }

    @Override
    protected int getMaxContainersCount() {
        return 4;
    }
}
