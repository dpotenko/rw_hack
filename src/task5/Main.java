package task5;

import task5.container.Cone;
import task5.container.Cube;
import task5.container.Cylinder;
import task5.liquid.Mercury;
import task5.liquid.Water;
import task5.ship.Ferry;
import task5.ship.OneDeckFerry;
import task5.ship.TwoDeckFerry;

public class Main {
    public static void main(String[] args) {
        Harbor harbor = new Harbor();
        Ferry twoDeckFerry = new TwoDeckFerry();

        twoDeckFerry.addContainer(new Cube(4, 2, new Water()));
        twoDeckFerry.addContainer(new Cone(4, 2, new Mercury()));

        System.out.println(twoDeckFerry.getTotalWeight());

        Ferry oneDeckFerry = new OneDeckFerry();
        oneDeckFerry.addContainer(new Cylinder(50, 50, new Water()));
        oneDeckFerry.addContainer(new Cone(100, 51, new Water()));
        oneDeckFerry.addContainer(new Cone(100, 51, new Water()));
        oneDeckFerry.addContainer(new Cone(50, 50, new Water()));
        oneDeckFerry.addContainer(new Cone(50, 50, new Water()));
        oneDeckFerry.addContainer(new Cone(50, 50, new Water()));
        oneDeckFerry.addContainer(new Cone(50, 50, new Water()));


        System.out.println(oneDeckFerry.getTotalWeight());

        harbor.addFerry(twoDeckFerry);
        harbor.addFerry(oneDeckFerry);
    }
}
