package task5.container;

import task5.liquid.Liquid;

public abstract class Container {

    protected int height;
    protected int diagonal;

    protected Liquid liquid;

    public Container(int diagonal, int height, Liquid liquid) {
        this.diagonal = diagonal;
        this.height = height;
        this.liquid = liquid;
    }

    public boolean isBig() {
        return diagonal >= 20 && height > 50;
    }

    public abstract double calculateWeight();

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
