package task5.container;

import task5.liquid.Liquid;

public class Cylinder extends Container {
    public Cylinder(int diagonal, int height, Liquid liquid) {
        super(diagonal, height, liquid);
    }

    @Override
    public double calculateWeight() {
        return Math.PI * Math.pow((double) diagonal / 2, 2) * height * liquid.getDensity();
    }
}
