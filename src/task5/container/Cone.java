package task5.container;

import task5.liquid.Liquid;

public class Cone extends Container {
    public Cone(int diagonal, int height, Liquid liquid) {
        super(diagonal, height, liquid); //конструируем предка, то чем являемся
    }

    @Override
    public double calculateWeight() {
        return 1.0 / 3.0 * Math.PI * Math.pow((double) diagonal / 2, 2) * height * liquid.getDensity();
    }
}
