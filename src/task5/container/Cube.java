package task5.container;

import task5.liquid.Liquid;

public class Cube extends Container {
    public Cube(int diagonal, int height, Liquid liquid) {
        super(diagonal, height, liquid);
    }

    @Override
    public double calculateWeight() {
        return Math.pow(diagonal, 2) / 2 * height * liquid.getDensity();
    }
}
