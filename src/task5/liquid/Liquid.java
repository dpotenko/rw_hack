package task5.liquid;

public interface Liquid {
    int getDensity();
}
