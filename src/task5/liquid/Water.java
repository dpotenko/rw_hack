package task5.liquid;

public class Water implements Liquid {
    @Override
    public int getDensity() {
        return 1000;
    }
}
