package task5.liquid;

public class Mercury implements Liquid {
    @Override
    public int getDensity() {
        return 2000;
    }
}
