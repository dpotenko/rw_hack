package task5;

import task5.ship.Ferry;

import java.util.ArrayList;
import java.util.List;

public class Harbor {
    List<Ferry> ferries = new ArrayList<>();

    public void addFerry(Ferry ferry) {
        ferries.add(ferry);
    }
}
