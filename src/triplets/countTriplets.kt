package triplets

// Complete the countTriplets function below.
data class Node(
    var pairs: Long = 0,
    var itself: Long = 1,
    var result: Long = 0
) {
    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}

fun countTriplets(arr: Array<Long>, r: Long): Long {
    val map = HashMap<Long, Node>()
    var result = 0L
    arr.forEachIndexed { index, element ->
        var node = map[element]
        if (node == null) {
            node = Node()
            map[element] = node
        } else {
            node.itself++
        }
        if (element % r == 0L) {
            /*if (r == 1L && node.itself >= 3) {
                if (node.itself == 3L) {
                    node.result = 1
                } else node.result = node.result * (node.itself / (node.itself - 3L))
            }*/
            if (map[element / r] != null && r != 1L) {
                map[element]?.pairs = map[element]?.pairs?.plus(map[element / r]?.itself ?: 0) ?: 0
                result += map[element / r]?.pairs ?: 0
            }
        }
    }
    if (r == 1L) {

        map.values.forEach {
            if (it.itself >= 3) {

                result += (it.itself * (it.itself - 1) * (it.itself - 2)) / 6
            }
        }
    }

    return result
}

fun factorial(n: Long): Long {
    var result = 1L
    for (i in 2..n)
        result *= i
    return result
}

fun main(args: Array<String>) {
    val nr = readLine()!!.trimEnd().split(" ")

    val n = nr[0].toInt()

    val r = nr[1].toLong()

    val arr = readLine()!!.trimEnd().split(" ").map { it.toLong() }.toTypedArray()

    val ans = countTriplets(arr, r)

    println(ans)
}
