package yulia;

public class Detail implements Comparable<Detail> {
    private Integer number;
    private Integer firstMachineTime;
    private Integer secondMachineTime;

    public Detail(Integer number, Integer firstMachineTime, Integer secondMachineTime) {
        this.number = number;
        this.firstMachineTime = firstMachineTime;
        this.secondMachineTime = secondMachineTime;
    }

    @Override
    public int compareTo(Detail anotherDetail) {
        boolean isThisShouldGoInHead = firstMachineTime <= secondMachineTime;
        boolean isAnotherShouldGoInHead = anotherDetail.firstMachineTime <= anotherDetail.secondMachineTime;
        if (isThisShouldGoInHead && isAnotherShouldGoInHead) {
            return firstMachineTime.compareTo(anotherDetail.firstMachineTime);
        } else if (isThisShouldGoInHead) {
            return -1;
        } else if (isAnotherShouldGoInHead) {
            return 1;
        }
        return -secondMachineTime.compareTo(anotherDetail.secondMachineTime);
    }

    @Override
    public String toString() {
        return  number +
                " (" + firstMachineTime +
                "," + secondMachineTime +
                ')';
    }
}


