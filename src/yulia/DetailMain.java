/*
package yulia;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import model.*;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

public class DetailMain {
    public static void main(String[] args) throws InterruptedException, IOException, TemplateException, JsonMappingException, URISyntaxException {

        Configuration configuration = new Configuration(Configuration.VERSION_2_3_30);
        configuration.setDefaultEncoding("UTF-8");
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        configuration.setLogTemplateExceptions(false);
        configuration.setWrapUncheckedExceptions(true);

        Template template = new Template("name", new FileReader(new File(DetailMain.class.getClassLoader().getResource("recipe.ftl").toURI())),
                configuration);

        RecipeRequest recipeRequest = new RecipeRequest("https://www.ah.nl/",
                "Sven",
                "dmitry.potenko@ah.nl",
                "Matthijs",
                "matthijs.potenko@ah.nl",
                "https://www.ah.nl/allerhande/recept/R-R1193853/tortilla-s-met-zoete-aardappel-avocado-en-pulled-vega-stukjes",
                "Test message",
                "https://static.ah.nl/assets/allerhande/_ui/allerhande/latest/assets/images",
                new Recipe(new RecipeImage(Map.of("445x297_JPG", "https://static.ah.nl/static/recepten/img_RAM_PRD131908_445x297_JPG.jpg")),
                        Arrays.asList("hoofdgerecht"), 4, "personen", new Nutritions(875), new RecipeTime(55), "Tortilla's met zoete aardappel, avocado &amp; pulled vega stukjes"));

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            recipeRequest = objectMapper.readValue(new FileReader(new File(DetailMain.class.getClassLoader().getResource("model.json").toURI())), RecipeRequest.class);
        } catch (JsonParseException | JsonMappingException e) {
            System.out.println("There was an error parsing request body "+ ExceptionUtils.getStackTrace(e));
        }


        //template.process(Map.of("recipeRequest", recipeRequest), new OutputStreamWriter(System.out));
        */
/*Observable.defer(() -> {
            throw new RuntimeException();
        })
                .doOnError(System.out::println)

                .retryWhen(errors -> errors.zipWith(Observable.range(1, 3), (n, i) -> new AbstractMap.SimpleEntry<Integer, Throwable>(i, n))
                        .flatMap(entry-> {
                            if (entry.getKey()<3) {
                                return Observable.timer(4, TimeUnit.SECONDS);
                            }
                            return Observable.error(entry.getValue());
                        }))
                .toBlocking()
        .first();*//*

*/
/*
        errors -> errors.zipWith(Observable.range(1, 3), (n, i) -> i)
                .flatMap(thr)
*//*

    }

}
*/
