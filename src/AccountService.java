import java.math.BigDecimal;

public class AccountService {
    public void transfer(Account from, Account to, BigDecimal amount) {
        synchronized (from) {
            synchronized (to) {
                from.subtract(amount);
                to.receive(amount);
            }
        }
    }
}
