package package2;

public class DigitalWalletTransaction {
    public void addMoney(DigitalWallet digitalWallet, int amount) {
        validateAuthorized(digitalWallet);
        validateAmount(amount);

        digitalWallet.setWalletBalance(digitalWallet.getWalletBalance() + amount);
    }


    public void payMoney(DigitalWallet digitalWallet, int amount) {
        validateAuthorized(digitalWallet);
        validateAmount(amount);
        validateBalance(digitalWallet, amount);

        digitalWallet.setWalletBalance(digitalWallet.getWalletBalance() - amount);
    }

    private void validateBalance(DigitalWallet digitalWallet, int amount) {
        if (digitalWallet.getWalletBalance() < amount) {
            throw new TransactionException("INSUFFICIENT_BALANCE", "Insufficient balance");
        }
    }

    private void validateAmount(int amount) {
        if (amount <= 0) {
            throw new TransactionException("INVALID_AMOUNT", "Amount should be greater than zero");
        }
    }

    private void validateAuthorized(DigitalWallet digitalWallet) {
        if (digitalWallet.getUserAccessCode() == null || digitalWallet.getUserAccessCode().isEmpty()) {
            throw new TransactionException("USER_NOT_AUTHORIZED", "User not authorized");
        }
    }
}
