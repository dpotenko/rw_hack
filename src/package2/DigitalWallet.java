package package2;

public class DigitalWallet {
    private final String walletId;
    private final String username;
    private final String userAccessCode;
    private int walletBalance;

    public DigitalWallet(String walletId, String username, String userAccessCode) {
        this.walletId = walletId;
        this.username = username;
        this.userAccessCode = userAccessCode;
    }

    public DigitalWallet(String walletId, String username) {
        this.walletId = walletId;
        this.username = username;
        this.userAccessCode = null;
    }

    public String getWalletId() {
        return walletId;
    }

    public String getUsername() {
        return username;
    }

    public int getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(int walletBalance) {
        this.walletBalance = walletBalance;
    }

    public String getUserAccessCode() {
        return userAccessCode;
    }
}
