package package4;

import com.google.gson.Gson;

import java.io.*;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class Solution {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d-MMMM-yyyy");
    private static Gson gson = new Gson();

    /*
     * Complete the function below.
     */
    static void openAndClosePrices(String firstDate, String lastDate, String weekDay) {

        try {


            List<String> daysOfWeek = Arrays.asList("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            int weekDayInt = daysOfWeek.indexOf(weekDay) + 1;

            Date firstParsedDate = simpleDateFormat.parse(firstDate);
            Date lastParsedDate = simpleDateFormat.parse(lastDate);

            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(firstParsedDate);

            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(lastParsedDate);

            while (startCalendar.getTime().compareTo(endCalendar.getTime()) < 0) {
                int currentDayOfWeek = startCalendar.get(Calendar.DAY_OF_WEEK);
                int offset = 7;
                String format = simpleDateFormat.format(startCalendar.getTime());
                if (currentDayOfWeek != weekDayInt) {
                    if (weekDayInt - currentDayOfWeek < 0) {
                        offset = 7 - currentDayOfWeek + weekDayInt;
                    } else {
                        offset = weekDayInt - currentDayOfWeek;
                    }
                } else if (currentDayOfWeek == weekDayInt) {

                    sendPreciseDate(format);
                }
                startCalendar.add(Calendar.DAY_OF_WEEK, offset);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void sendPreciseDate(String date) throws IOException {
        URL url = new URL("https://jsonmock.hackerrank.com/api/stocks/?date=" + date);
        BufferedReader stream = new BufferedReader(new InputStreamReader(url.openStream()));
        StocksResponse stocksResponse = gson.fromJson(stream, StocksResponse.class);
        printResult(stocksResponse);
    }

    private static void printResult(StocksResponse stocksResponse) {
        for (StockData stockData : stocksResponse.getData()) {
            System.out.println(stockData.getDate() + " " + stockData.getOpen() + " " + stockData.getClose());
        }
    }

    public static void main(String[] args) throws IOException, ParseException {



        final BufferedReader in = new BufferedReader(new FileReader(new File("input.txt")));
        String _firstDate;
        try {
            _firstDate = in.readLine();
        } catch (Exception e) {
            _firstDate = null;
        }

        String _lastDate;
        try {
            _lastDate = in.readLine();
        } catch (Exception e) {
            _lastDate = null;
        }

        String _weekDay;
        try {
            _weekDay = in.readLine();
        } catch (Exception e) {
            _weekDay = null;
        }

        openAndClosePrices(_firstDate, _lastDate, _weekDay);

    }
}
